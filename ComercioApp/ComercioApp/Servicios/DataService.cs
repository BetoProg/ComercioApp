﻿using ComercioApp.Data;
using ComercioApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComercioApp.Servicios
{
    public class DataService
    {
        public Response InsertUser(User user)
        {
            try
            {
                using (var da = new DataAcces())
                {
                    var oldUser = da.First<User>(false);
                    if (oldUser != null)
                    {
                        da.Delete(oldUser);
                    }
                    da.Insert(user);
                }

                return new Response
                {
                    IsSucces = true,
                    Message = "se ha ingresado correctamente",
                    Result = user
                };
            }
            catch (Exception ex)
            {

                return new Response
                {
                    IsSucces = false,
                    Message = ex.Message
                };
            }
        }
    }
}
