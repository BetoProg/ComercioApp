﻿using ComercioApp.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ComercioApp.Servicios
{
    public class NavigationService
    {
        public async Task Navigate(string pageName)
        {
            switch (pageName)
            {
                case "ClientesPage":
                    await App.Navigator.PushAsync(new ClientesPage());
                    break;
                case "ProductoPage":
                    await App.Navigator.PushAsync(new ProductoPage());
                    break;
                case "EntregasPage":
                    await App.Navigator.PushAsync(new EntregasPage());
                    break;
                case "PedidosPage":
                    await App.Navigator.PushAsync(new PedidosPage());
                    break;
                case "SyncPage":
                    await App.Navigator.PushAsync(new SyncPage());
                    break;
                case "UserPage":
                    await App.Navigator.PushAsync(new UserPage());
                    break;
                case "ConfigPage":
                    await App.Navigator.PushAsync(new ConfigPage());
                    break;
            }
        }

        internal void SetMainPage()
        {
            App.Current.MainPage = new MaterPage();
        }
    }
}
