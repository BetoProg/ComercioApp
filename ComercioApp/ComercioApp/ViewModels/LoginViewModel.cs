﻿using ComercioApp.Data;
using ComercioApp.Models;
using ComercioApp.Servicios;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;

namespace ComercioApp.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        #region Atributos
        private NavigationService navigationService;
        private DialogService dialogService;
        private ApiService apiService;
        private DataService dataservice;
        private bool isRunning;
        #endregion

        #region Eventos
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Propiedades
        public string User { get; set; }

        public string Password { get; set; }

        public bool IsRemembred { get; set; }

        public bool IsRunning {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("IsRunning"));
                    }
                }
            }
            get
            {
                return isRunning;
            }
        }
        #endregion

        #region Contructores
        public LoginViewModel()
        {
            navigationService = new NavigationService();
            dialogService = new DialogService();
            apiService = new ApiService();
            dataservice = new DataService();
        } 
        #endregion

        #region Comandos
        public ICommand LoginCommand { get { return new RelayCommand(Login); } }

        private async void Login()
        {
            if (string.IsNullOrEmpty(User)){
                await dialogService.ShowMessage("Error", "Debes de ingresar un usuario");
                return;
            }
            if (string.IsNullOrEmpty(Password))
            {
                await dialogService.ShowMessage("Error", "Debes de ingresar una contrasena");
                return;
            }
            IsRunning = true;
            var response = await apiService.Login(User,Password);
            IsRunning = false;
            if (response.IsSucces)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            var user = (User)response.Result;
            user.IsRemember = IsRemembred;
            user.Password = Password;

            navigationService.SetMainPage();
        } 
        #endregion
    }
}
