﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ComercioApp.Servicios
{
    public class DialogService
    {
        public async Task ShowMessage(string titulo,string mensaje)
        {
            await App.Current.MainPage.DisplayAlert(titulo, mensaje,"Aceptar");
        }
    }
}
