﻿using ComercioApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ComercioApp.Servicios
{
    public class ApiService
    {
        public async Task<Response> Login(string email, string password) {
            try
            {
                var loginRequest = new LoginRequest {
                    Email = email,
                    Password = password
                };
                var request = JsonConvert.SerializeObject(loginRequest);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://griegos-soft.com");
                var uri = "/EComerce/api/User/Login";
                var response = await client.PostAsync(uri,content);

                if (!response.IsSuccessStatusCode) {
                    return new Response {
                        IsSucces = true,
                        Message = "Usuario o contraseña incorrectos"
                    };
                }

                var result = await response.Content.ReadAsStringAsync();
                var user = JsonConvert.DeserializeObject<User>(result);

                return new Response
                {
                    IsSucces = true,
                    Message = "Login Ok",
                    Result = user,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucces = false,
                    Message = ex.Message
                };
            }
        }
    }
}
