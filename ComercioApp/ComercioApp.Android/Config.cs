using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ComercioApp.Interfaces;
using SQLite.Net.Interop;
using Xamarin.Forms;

[assembly: Dependency(typeof(ComercioApp.Droid.Config))]
namespace ComercioApp.Droid
{

    public class Config : IConfig
    {
        private string directoryDB;
        private ISQLitePlatform plataform;

        public string DirectoryDB {
            get
            {
                if (string.IsNullOrEmpty(directoryDB))
                {
                    directoryDB = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                return directoryDB;
            }
        }

        public ISQLitePlatform Plataform {
            get
            {
                if (plataform == null)
                {
                    plataform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                }
                return plataform;
            }
        }
    }
}