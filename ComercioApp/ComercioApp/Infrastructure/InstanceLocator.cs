﻿using ComercioApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComercioApp.Infrastructure
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; }

        public InstanceLocator()
        {
            Main = new MainViewModel();
        }

    }
}
